using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovPlayerAuto : MonoBehaviour
{
    public float moveSpeed = 5f;  

    private bool isMovingUp = false; 

    private void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {

            isMovingUp = !isMovingUp;
        }

        if (isMovingUp)
        {
            transform.Translate(Vector3.up * moveSpeed * Time.deltaTime);
        }
        else
        {
            transform.Translate(Vector3.down * moveSpeed * Time.deltaTime);
        }
    }
}
