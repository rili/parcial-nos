using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnAutos : MonoBehaviour
{
    [SerializeField] private float Tiempospawn = 2f;
    [SerializeField] private GameObject Auto;
    [SerializeField] private int Cantidad = 5;
    [SerializeField] private float PosXSpawn = 12f;
    [SerializeField] private float MinYpos = -2f;
    [SerializeField] private float MaxYpos = 3f;
    private GameObject[] autos;
    private int ContadorAuto;
    private float TiempoPasado;

    private void Start()
    {
        autos = new GameObject[Cantidad];
        for (int i = 0; i < Cantidad; i++)
        {
            autos[i] = Instantiate(Auto);
            autos[i].SetActive(false);
        }
    }
    void Update()
    {
        TiempoPasado += Time.deltaTime;

        if (TiempoPasado > Tiempospawn)
        {
            SpawnObstacle();
        }

    }

    private void SpawnObstacle()
    {
        TiempoPasado = 0;

        float PosYSpawn = Random.Range(MinYpos, MaxYpos);
        Vector3 Spawnpos = new Vector3(PosXSpawn, PosYSpawn, 3.39f);
        autos[ContadorAuto].transform.position = Spawnpos;

        if (!autos[ContadorAuto].activeSelf)
        {
            autos[ContadorAuto].SetActive(true);
        }
        ContadorAuto++;

        if (ContadorAuto == Cantidad)
        {
            ContadorAuto = 0;
        }
    }
}
