using UnityEngine;

public class EditorDeNivel : MonoBehaviour
{
    public Texture2D[] mapas;
    public ColorAPrefab[] colorMappings;
    public int z;
    public int xoffset;
    public int yoffset;
    public Texture2D mapa;

    void Start()
    {
        int mapaSeleccionada = Random.Range(0, mapas.Length);
        mapa = mapas[mapaSeleccionada];
        GenerarNivel();
    }

    private void GenerarNivel()
    {
        for (int x = 0; x < mapa.width; x++)
        {
            for (int y = 0; y < mapa.height; y++)
            {
                GenerarTile(x, y, z);
            }
        }
    }

    void GenerarTile(int x, int y, int z)
    {
        Color pixelColor = mapa.GetPixel(x, y);

        if (pixelColor.a == 0)
        {
            return;
        }

        foreach (ColorAPrefab colorMapping in colorMappings)
        {
            if (colorMapping.color.Equals(pixelColor))
            {
                Vector3 position = new Vector3(x + xoffset, y + yoffset, z);
                Instantiate(colorMapping.prefab, position, Quaternion.identity, transform);
            }
        }
    }
}
