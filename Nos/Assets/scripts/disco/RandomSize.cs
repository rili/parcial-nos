using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomSize : MonoBehaviour
{
    public Vector3 minSize = new Vector3(1f, 1f, 1f);
    public Vector3 maxSize = new Vector3(5f, 5f, 5f);

    private void Start()
    {
        Vector3 randomSize = new Vector3(
            Random.Range(minSize.x, maxSize.x),
            Random.Range(minSize.y, maxSize.y),
            Random.Range(minSize.z, maxSize.z)
        );

        transform.localScale = randomSize;
    }
}
