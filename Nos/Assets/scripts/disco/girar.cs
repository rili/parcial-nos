using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class girar : MonoBehaviour
{
    public static int velocidad;
    public int x;
    public int y;
    public int z;
    public int minVelocidad;
    public int maxVelocidad;

    void Start()
    {
        velocidad = Random.Range(minVelocidad, maxVelocidad);
    }

    void Update()
    {
        transform.Rotate(new Vector3(x, y, z) * Time.deltaTime * velocidad);
    }

    void OnMouseDown()
    {
        velocidad = 0;
    }
}
