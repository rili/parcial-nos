using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rectangulo : MonoBehaviour
{
    public float moveSpeed = 5f;
    public float moveLimit = 10f;

    private bool isMovingRight = false;
    private Vector3 initialPosition;

    private void Start()
    {
        initialPosition = transform.position;
    }

    private void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            isMovingRight = !isMovingRight;
        }

        if (isMovingRight)
        {
            if (transform.position.x < initialPosition.x + moveLimit)
            {
                transform.Translate(Vector3.right * moveSpeed * Time.deltaTime);
            }
        }
        else
        {
            if (transform.position.x > initialPosition.x - moveLimit)
            {
                transform.Translate(Vector3.left * moveSpeed * Time.deltaTime);
            }
        }
    }
}






