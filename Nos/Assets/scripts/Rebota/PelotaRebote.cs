using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PelotaRebote : MonoBehaviour
{
    public float fuerzaRebote = 10f;
    private Rigidbody rb;

    private void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("pared"))
        {
            Vector3 direccionRebote = CalculateReboteDirection(other.transform);
            rb.AddForce(direccionRebote * fuerzaRebote, ForceMode.Impulse);
        }
    }

    private Vector3 CalculateReboteDirection(Transform objectTransform)
    {
        Vector3 direccion = objectTransform.position - transform.position;
        direccion.y = 0f;
        return direccion.normalized;
    }
}
