using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParedRebote : MonoBehaviour
{
    public float trampolineForce;

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("pelota"))
        {
            Rigidbody rigidbody = collision.gameObject.GetComponent<Rigidbody>();
            rigidbody.AddForce(new Vector3(0, 10 * trampolineForce, 0), ForceMode.Impulse);
        }
        OnCollisionEnter(collision);
    }
}
